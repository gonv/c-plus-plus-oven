/* 
 * File:   forno.h
 * Author: Marina
 *
 * Created on 3 de Junho de 2019, 20:59
 */

#ifndef FORNO_H 
#define	FORNO_H 

#include "comida.h"
#include <map>
 
class Forno{ 
 
    bool estado; 
    float quantidade_de_comida; 
    int tempo; 
    int temperatura_real; 
    int temperatura_pretendida; 
    float capacidade;
    int temperatura_maxima;
    std::vector<int> pratos_dentro_do_forno;

    //mapa dos pratos
    std::map<int,Comida *> comidas;
    
public: 
    Forno(int capacidade_do_forno, int max_temperatura); 
    Forno(const Forno & orig);
    Forno & operator = (const Forno & orig);
    
    //goal 1 
    void liga_desliga(int interruptor); 
    void escolher_temperatura(int temperatura_escolhida); 
    void tempo_reinicia(); 
    void escolhe_prato();
    void verifica_se_esta_ligado();
    void estado_dos_fornos(Forno * a);
    void inforno();
    
    //goal 2 
    void inserir_comida_no_forno(int n_prato); 
    void escolher_ingredientes_e_meter_comida_no_forno(int prato);
    
    //goal 5
    virtual void calcula_temperatura(int diferenca, int factor = 1);
    virtual void evolucao_da_temperatura(int tempo_actual, int n_prato, int * contador_queima);
    void tira_todos_os_pratos_de_dentro_do_forno();
    bool mostra_pratos_que_estao_dentro_do_forno();
    void tira_so_um_prato_do_forno(std::string nome_do_prato);
    
    virtual ~Forno(); 
    
}; 
 
 
#endif	/* FORNO_H */ 
 


