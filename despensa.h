/* 
 * File:   despensa.h
 * Author: Marina
 *
 * Created on 8 de Junho de 2019, 12:00
 */

#ifndef DESPENSA_H
#define	DESPENSA_H

#include "forno.h" 

#include <chrono>

    
void mostra_pratos_de_cada_menu(int numero_do_menu);
int escolhe_forno();
void menu_dos_ingredientes(Comida * a);
void ignora_se_incompativel();
std::string converter_para_string(char* array, int tamanho);

#endif	/* DESPENSA_H */

