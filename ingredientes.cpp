/* 
 * File:   Ingrediente.cpp
 * Author: Marina
 * 
 * Created on 8 de Junho de 2019, 14:54
 */

#include "ingredientes.h"

using namespace std;

Ingrediente::Ingrediente(const string nome, const string tipo, float preco_venda): nome_do_ingrediente(nome), tipo_de_ingrediente(tipo), preco(preco_venda){

}


string Ingrediente::devolve_nome_ingrediente(){
    
    return nome_do_ingrediente;
}


string Ingrediente::devolve_tipo_ingrediente(){
    
    return tipo_de_ingrediente;
}


float Ingrediente::devolve_preco(){
    
    return preco;
}


string Ingrediente::mostra_info_ingrediente(){
    ostringstream os;

    os << "Nome do ingrediente: " << nome_do_ingrediente << "\n" 
       << "Tipo de ingrediente: " << tipo_de_ingrediente << "\n"
       << "Preço: " << preco << " €" << endl;
    
    return os.str();
}


Ingrediente::~Ingrediente() {

}

