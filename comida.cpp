/* 
 * File:   Comida.cpp
 * Author: Marina
 * 
 * Created on 6 de Junho de 2019, 16:52
 */

#include "comida.h"

using namespace std;

Comida::Comida(string prato, string tipo, float peso, int confeccionado, int queimado){

    nome_do_prato = prato;
    temperatura_confeccao = confeccionado;
    temperatura_queima = queimado;
    quantidade_de_comida = peso;
    tipo_de_comida = tipo;
    
}

Comida::Comida(const Comida & a){
    if(this != &a){
        
        this->nome_do_prato = a.nome_do_prato;
        this->tipo_de_comida = a.tipo_de_comida;
        this->temperatura_confeccao = a.temperatura_confeccao;
        this->temperatura_queima = a.temperatura_queima;
        this->quantidade_de_comida = a.quantidade_de_comida;   //peso da comida em kg
        this->preco_do_prato = a.preco_do_prato;
        
        for(auto c:a.ingredientes)
            ingredientes.push_back(new Ingrediente(*c));
    }
}

void Comida::mostrar_prato(){   // mostraPrato
    
    cout << "Nome do prato: " << nome_do_prato << std::endl;
    cout << "Tipo de comida: " << tipo_de_comida << std::endl;
    cout << "Quantidade de comida: " << quantidade_de_comida << std::endl;
    cout << "Temperatura de confeccao: " << temperatura_confeccao << std::endl;
    cout << "Temperatura de cremação: " << temperatura_queima << std::endl;
    
}


string Comida::estado_da_confeccao(int tempo, int temperatura) {
    
    if (temperatura < temperatura_queima && tempo < 3) {
        return "ainda está cru!";
    } else if (temperatura < temperatura_queima) {
        return "a assar...";
    } else if (temperatura >= temperatura_queima) {
        return "queimado...";
    }

}


float Comida::devolve_peso_da_comida(){

    return quantidade_de_comida;
}


void Comida::criar_ingredientes(string nome, string tipo, float preco_venda){
    
    Ingrediente * novo_ingrediente = new Ingrediente(nome, tipo, preco_venda);
    ingredientes.push_back(novo_ingrediente);
    
}


bool Comida::procurar_um_ingrediente(string nome, string tipo){
    
    for(int i=0; i<ingredientes.size(); i++){
        
        if((*ingredientes[i]).devolve_nome_ingrediente() == nome && (*ingredientes[i]).devolve_tipo_ingrediente() == tipo){
            return true;    
        }
    }
    
    return false;  
}


void Comida::apaga_um_ingrediente(string nome_do_ingrediente){
    
    auto it = ingredientes.begin();
    
    
    while(it != ingredientes.end()){
        
        if((*it)->devolve_nome_ingrediente() == nome_do_ingrediente){
            
            delete *it;
            it = ingredientes.erase(it);
            return;
        }
        else{
            it++;
        }
    }
    
}

void Comida::mostra_infos_dos_ingredientes(){
    
    string inforno_dos_ingredientes;
    
    for(int i=0; i<ingredientes.size(); i++){
        
        inforno_dos_ingredientes = (*ingredientes[i]).mostra_info_ingrediente();
        cout << inforno_dos_ingredientes << endl;
    }
    
}

void Comida::calcula_preco_do_prato(){
    
    float soma = 0;
    
    for(int i=0; i<ingredientes.size(); i++){
        
        soma = soma + (*ingredientes[i]).devolve_preco();    
        
    }
    
    soma = soma * 10; //passar de preço por 100g a preço por kg
    
    preco_do_prato = quantidade_de_comida * soma;
}

bool Comida::confirma_se_tem_pelo_menos_um_ingrediente(){
    if(ingredientes.size() > 0){
        return true;
    }
    return false;
}

float Comida::devolde_preco_prato(){
    
    return preco_do_prato;
}

string Comida::devolve_nome_do_prato(){
    return nome_do_prato;
}

Comida::~Comida() {
    for(int i=0; i<ingredientes.size(); i++){
        delete ingredientes[i];
    }
}


