
#include "forno.h" 
#include "fornolenha.h" 
#include "despensa.h"
 
using namespace std; 
 
 
int main(int argc, char** argv) { 
    
    Forno *a = new Forno(30.0, 400); 
    FornoLenha *b = new FornoLenha(30.0, 400); 
    
    
    //Forno *c = new Forno(30.0, 400);
    
    //*c = *a;
    
    bool done; 
    char ch;
    int prato_escolhido = 0;
    int prato_escolhido_um = 0;
    int prato_escolhido_dois = 0;
    int conta_queimado_um = 0;
    int conta_queimado_dois = 0;
    
    
    done = false; 
    cout << "O que deseja fazer?" << endl; 
 
    while (!done) { 
        cout << "\nOpções" << endl; 
        cout << "A - Ligar ou desligar o forno" << endl; 
        cout << "C - Escolher e inserir comida do forno" << endl; 
        cout << "N - Novidades sobre o processamento" << endl; 
        cout << "E - Eliminar um prato de um forno" << endl;
        cout << "R - Eliminar a comida do forno e reiniciar" << endl; 
        cout << "T - Escolher a temperatura" << endl; 
        cout << "Q - Sair" << endl; 
        cin >> ch; 
        ch = toupper(ch); 
        switch (ch) { 
            case 'A': 
            {
                int forno_escolhido_a = 0;
                a->estado_dos_fornos(b);
                forno_escolhido_a = escolhe_forno();
                cout << "Se pretender desligar, prima 0. Se pretender ligar, prima 1." << endl;
                int interruptor = 0;
                cin >> interruptor;
                while (!cin.good()){
                    ignora_se_incompativel();
                    cin >> interruptor;
                }
                if(forno_escolhido_a == 1) {
                    a->liga_desliga(interruptor);
                    a->verifica_se_esta_ligado();
                }
                else if(forno_escolhido_a == 2) {
                    b->liga_desliga(interruptor);
                    b->verifica_se_esta_ligado();
                }
                break; 
            }
            case 'C':
            { 
                cout << "Que menu prefere?" << endl; 
                cout << "1 - Fast food" << endl; 
                cout << "2 - Indiano" << endl; 
                cout << "3 - Portugues\n" << endl; 
                int escolha_menu = 0;
                cin >> escolha_menu; 
                while(!cin.good() && (escolha_menu <= 0 || escolha_menu > 3)){ 
                    ignora_se_incompativel();
                    cin >> escolha_menu; 
                } 
                mostra_pratos_de_cada_menu(escolha_menu);
                cin >> prato_escolhido; 
                while(1){
                    if(escolha_menu == 1 && (prato_escolhido > 0 && prato_escolhido <= 4))
                        break;
                    else if(escolha_menu == 2 && (prato_escolhido >= 5 && prato_escolhido <= 7))
                        break;
                    else if(escolha_menu == 3 && (prato_escolhido >= 8 && prato_escolhido <= 10))
                        break;
                    else{
                        cout << "Resposta inválida! Escolha uma das opções!" << endl; 
                        cin >> prato_escolhido;
                    }
                } 
                int forno_escolhido_c = 0;
                forno_escolhido_c = escolhe_forno();
                if(forno_escolhido_c == 1){
                    prato_escolhido_um = prato_escolhido;
                    a->escolher_ingredientes_e_meter_comida_no_forno(prato_escolhido_um);
                }
                else if(forno_escolhido_c == 2){
                    prato_escolhido_dois = prato_escolhido;
                    b->escolher_ingredientes_e_meter_comida_no_forno(prato_escolhido_dois);
                }
                break;
            }
            case 'N': 
            {
                int tempo_actual; 
                cout << "Quanto tempo já passou desde que introduziu a comida (em minutos)?" << endl; 
                cin >> tempo_actual; 
                while (!cin.good()){
                    ignora_se_incompativel();
                    cin >> tempo_actual; 
                }
                int forno_escolhido_n = 0;
                forno_escolhido_n = escolhe_forno();
                if(forno_escolhido_n == 1){
                    a->evolucao_da_temperatura(tempo_actual, prato_escolhido_um, &conta_queimado_um);
                }
                else if(forno_escolhido_n == 2){
                    b->evolucao_da_temperatura(tempo_actual, prato_escolhido_dois, &conta_queimado_dois);
                }
                break; 
            }
            case 'E':
            {
                int forno_escolhido_e = 0;
                forno_escolhido_e = escolhe_forno();
                if(forno_escolhido_e == 1){
                    if(a->mostra_pratos_que_estao_dentro_do_forno()){
                        cin.ignore(numeric_limits<streamsize>::max(),'\n');
                        string nome_do_prato_a_eliminar_a;
                        getline(cin, nome_do_prato_a_eliminar_a);
                        a->tira_so_um_prato_do_forno(nome_do_prato_a_eliminar_a);
                    }
                }
                else if(forno_escolhido_e == 2){
                    if(b->mostra_pratos_que_estao_dentro_do_forno()){
                        cin.ignore(numeric_limits<streamsize>::max(),'\n');
                        string nome_do_prato_a_eliminar_b;
                        getline(cin, nome_do_prato_a_eliminar_b);
                        b->tira_so_um_prato_do_forno(nome_do_prato_a_eliminar_b);
                    }
                }
                break;
            }
            case 'R': 
            {
                int forno_escolhido_r = 0;
                forno_escolhido_r = escolhe_forno();
                if(forno_escolhido_r == 1){
                    prato_escolhido_um = 0;
                    conta_queimado_um = 0;
                    a->tira_todos_os_pratos_de_dentro_do_forno();
                    cout << "Comida do forno padrão eliminada com sucesso" << endl;
                    a->tempo_reinicia(); 
                }
                else if(forno_escolhido_r == 2){
                    prato_escolhido_dois = 0;
                    conta_queimado_dois = 0;
                    b->tira_todos_os_pratos_de_dentro_do_forno();
                    cout << "Comida do forno de lenha eliminada com sucesso" << endl;
                    b->tempo_reinicia(); 
                }
                break;
            }
            case 'T': 
            {
                int forno_escolhido_t = 0;
                forno_escolhido_t = escolhe_forno();
                cout << "Insira a temperatura pretendida:" << endl; 
                if(forno_escolhido_t == 1){
                    int temperatura_escolhida_um = 0;
                    cin >> temperatura_escolhida_um; 
                    while (!cin.good()){
                        ignora_se_incompativel();
                        cin >> temperatura_escolhida_um; 
                    }
                    a->escolher_temperatura(temperatura_escolhida_um);    
                }
                else if(forno_escolhido_t == 2){
                    int temperatura_escolhida_dois = 0;
                    cin >> temperatura_escolhida_dois; 
                    while (!cin.good()){
                        ignora_se_incompativel();
                        cin >> temperatura_escolhida_dois; 
                    }
                    b->escolher_temperatura(temperatura_escolhida_dois);  
                }
                break; 
            }
            case 'Q': 
            {
                done = true; 
                break; 
            }
            default: 
                cout << "Resposta inválida" << endl; 
     
        } 
    } 
     
    cout << "\nBon appétit =)" << endl;     
    
    delete b;
    delete a;
    
    return 0; 
} 
 
