/* 
 * File:   forno.cpp
 * Author: Marina
 * 
 * Created on 3 de Junho de 2019, 20:59
 */

#include "forno.h" 
#include "despensa.h"

using namespace std;
 
 Forno::Forno(int capacidade_do_forno, int max_temperatura): capacidade(capacidade_do_forno), temperatura_maxima(max_temperatura){ 
      
    estado = false; 
    quantidade_de_comida = 0.0; 
    tempo = 0; 
    temperatura_real = 10; 
    temperatura_pretendida = 10; 
    comidas = {
        {0, new Comida("empty food", "fast food", 0.0, 10, 100)},
        {1, new Comida("empadas de frango", "fast food", 3.0, 100, 150)},
        {2, new Comida("pizza havaiana", "fast food", 3.0, 90, 150)},
        {3, new Comida("pizza quatro queijos", "fast food", 2.5, 100, 250)},
        {4, new Comida("pizza camponesa", "fast food", 1.0, 110, 150)},
        {5, new Comida("caril de borrego", "indiano", 2.5, 100, 250)},
        {6, new Comida("caril de frango", "indiano", 3.0, 110, 150)},
        {7, new Comida("chamuças vegetarianas", "indiano", 1.5, 150, 200)},
        {8, new Comida("bacalhau espiritual", "portugues", 3.0, 100, 110)},
        {9, new Comida("lombo de porco assado com ameixas", "portugues", 2.0, 90, 120)},
        {10, new Comida("polvo à lagareiro", "portugues", 2.5, 100, 150)}};
 } 

 
Forno::Forno(const Forno & orig) {
    *this = orig;
    
}


Forno & Forno::operator=(const Forno & orig){
    if(this != &orig){
        //Apagar tudo
   
        //apaga a memoria
    for(int k = 0; k <=comidas.size(); k++){
        delete comidas.find(k)->second;
    }
    
    //apaga info mapa
    comidas.clear();
    
    //cout << "erase\n" ;
      /*  for (auto &x : comidas)
        {
            x.first; // string (key)
            x.second->mostrar_prato();
        }*/
    //cout << "copy\n";
     comidas = orig.comidas;
    /*
        typedef map<int,Comida*>::const_iterator It;
        for (It p = orig.comidas.begin(); p!=orig.comidas.end(); ++p) 
        {
                comidas.insert({p->first,p->second });
        }
      */  
     /*
        for (auto &x : comidas)
        {
            x.first; // string (key)
            x.second->mostrar_prato();
        }
      */
        
       
    
        pratos_dentro_do_forno.clear();

        //Copiar tudo da origem
        this->estado = orig.estado; 
        this->quantidade_de_comida = orig.quantidade_de_comida; 
        this->tempo = orig.tempo; 
        this->temperatura_real = orig.temperatura_real;
        this->temperatura_pretendida = orig.temperatura_pretendida; 
        this->capacidade = orig.capacidade; 
        this->temperatura_maxima = orig.temperatura_maxima;


        return *this;
    }
}


void Forno::liga_desliga(int interruptor){ 
    
    this->estado = interruptor;
            
} 

void Forno::estado_dos_fornos(Forno * b){
    if(this->estado == true && b->estado == true){ 
        cout << "Ambos os fornos estão ligados" << endl; 
    } 
    else if(this->estado == true && b->estado == false){ 
        cout << "O forno 1 está ligado e o forno 2 está desligado" << endl; 
    } 
    else if(this->estado == false && b->estado == true){ 
        cout << "O forno 2 está ligado e o forno 1 está desligado" << endl; 
    } 
    else{ 
        cout << "Ambos os fornos estão desligados" << endl;  
    } 
    
}
void Forno::verifica_se_esta_ligado(){ 
    if(estado == true){ 
        cout << "O forno está ligado" << endl; 
    } 
    else{ 
        cout << "O forno está desligado" << endl;  
    } 
} 

void Forno::calcula_temperatura(int diferenca, int factor){
        if(estado==false){ 
            temperatura_real = temperatura_real - ((200 / (quantidade_de_comida + 10))/factor) * diferenca; 
            if(temperatura_real <= 10){ 
                 temperatura_real = 10; 
            }  
        } 
        else if(estado==true){ 
            temperatura_real = temperatura_real + ((200 / (quantidade_de_comida + 10))/factor) * diferenca; 
            if(temperatura_real >= temperatura_pretendida){ 
                temperatura_real = temperatura_pretendida; 
            } 
        }
}

void Forno::evolucao_da_temperatura(int tempo_actual, int n_prato, int * contador_queima){   
    
    if(tempo >= tempo_actual){ 
        cout << "O tempo não avançou" << endl; 
        return;
    } 
      
    int diferenca; 
      
     diferenca = tempo_actual - tempo; 
     if(diferenca<0){ 
         diferenca=0; 
     } 
      
    
    calcula_temperatura(diferenca); 

    tempo = tempo_actual;
    
    cout << "Novidades:" << endl;  
    if(estado == true){
        cout << "Estado: Ligado" << endl;  
    }
    else{
        cout << "Estado: Desligado" << endl; 
    }
    cout << "Tempo decorrido: " << tempo << endl;  
    cout << "Temperatura pretendida: " << temperatura_pretendida << endl;  
    cout << "Temperatura actual: " << temperatura_real << endl; 
    
    string estado_da_comida;
    if(pratos_dentro_do_forno.size() > 0){
        for(int i = 0; i < pratos_dentro_do_forno.size(); i++){
            estado_da_comida = (comidas.find(pratos_dentro_do_forno[i])->second)->estado_da_confeccao(tempo, temperatura_real);

            if(pratos_dentro_do_forno[i] != 0){
                if(estado_da_comida == "queimado..."){
                    *contador_queima = 1;
                }
                if(*contador_queima == 0){
                    cout << "Prato número " << pratos_dentro_do_forno[i] << " - Estado da comida: " << estado_da_comida << endl; 
                }
                else if(*contador_queima == 1){
                    cout << "Prato número " << pratos_dentro_do_forno[i] << " - Estado da comida: queimado..." << endl; 
                }
            }
        }
    }
    else{
        cout << "Não há comida no forno" << endl; 
    }
 } 
 
void Forno::inserir_comida_no_forno(int n_prato){ 
      
    float aux = 0.0;
    
    for(int i = 0; i < pratos_dentro_do_forno.size(); i++){
        aux += (comidas.find(pratos_dentro_do_forno[i])->second)->devolve_peso_da_comida();
    }
    aux += (comidas.find(n_prato)->second)->devolve_peso_da_comida();
    
    if(aux > capacidade){
        cout << "Excede a capacidade do forno!" << endl; 
    } 
    else if(aux <= 0){ 
        cout << "O forno ficou vazio" << endl; 
    } 
    else{
        cout << "Sucesso!" << endl;
        pratos_dentro_do_forno.push_back(n_prato);
        cout << "Há " << aux << " kg de comida no forno!\n" << endl;
    } 
    
} 
 
void Forno::escolher_temperatura(int temperatura_escolhida){ 
    if(estado == 1){
        
        if(temperatura_escolhida > temperatura_maxima){
            temperatura_pretendida = temperatura_maxima;
            cout << "Excede os limites suportados pelo forno! A temperatura utilizada será temperatura_maxima" << endl;
        }
        else if(temperatura_escolhida < 10){
            temperatura_pretendida = 10;
            cout << "A temperatura minima é 10ºC" << endl;
        }
        else{
            temperatura_pretendida = temperatura_escolhida;
        }
     
        cout << "A comida vai ser confeccionada a " << temperatura_pretendida << "ºC \n" << endl; 
    }
    else{
        cout << "É necessário ligar o forno primeiro" << endl;
    }
} 
 
void Forno::tempo_reinicia(){ 
    tempo=0; 
    cout << "O temporizador foi reinicado com sucesso!\n" << endl;     
} 


void Forno::escolher_ingredientes_e_meter_comida_no_forno(int n_prato){
    menu_dos_ingredientes(comidas.find(n_prato)->second);
    inserir_comida_no_forno(n_prato);
}


void Forno::inforno(){ 
 
    cout << "\nInformações importantes sobre o forno:" << endl; 
    cout << "Estado:" << (estado == true ? "ligado" : "desligado") << endl; 
     
    if(estado==true){ 
        cout << "Quantidade de comida dentro do forno:" << quantidade_de_comida << "kg" << endl; 
        cout << "Ultima actualização do tempo:" << tempo << "minutos" << endl; 
        cout << "Temperatura actual:" << temperatura_real << "ºC" << endl; 
        cout << "Temperatura pretendida:" << temperatura_pretendida << "ºC" << endl; 
        cout << "\n" << endl; 
    }   
} 


void Forno::tira_todos_os_pratos_de_dentro_do_forno(){
    pratos_dentro_do_forno.clear();
}


bool Forno::mostra_pratos_que_estao_dentro_do_forno(){
    if(pratos_dentro_do_forno.size() > 0){
        cout << "Indique o nome do prato que pretende eliminar:" << endl;

        for(int i = 0; i < pratos_dentro_do_forno.size(); i++){
            cout << (comidas.find(pratos_dentro_do_forno[i])->second)->devolve_nome_do_prato() << endl;
        }
        
        return true;
    }
    else{
        cout << "O forno está vazio!" << endl;
        return false;
    }
}


void Forno::tira_so_um_prato_do_forno(string nome_do_prato){
    
    auto it = pratos_dentro_do_forno.begin();
    
    while(it != pratos_dentro_do_forno.end()){
        
        if((comidas.find(*it)->second)->devolve_nome_do_prato() == nome_do_prato){
            
            it = pratos_dentro_do_forno.erase(it);
            cout << "Prato eliminado com sucesso!" << endl; 
            return;
        }
        else{
            it++;
        }
    }
    
    cout << "Esse prato não existe neste forno!" << endl; 
    
}


Forno::~Forno(){ 
    
    //apagar memoria dinamica dos pratos
    for(int k = 0; k <= comidas.size(); k++){
        delete comidas.find(k)->second;
    }
}

