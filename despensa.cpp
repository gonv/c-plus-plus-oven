#include "despensa.h"


using namespace std;
    
void mostra_pratos_de_cada_menu(int numero_do_menu){
    
    cout << "Escolha um dos pratos:" << endl;
    
    if (numero_do_menu == 1) { //fast food
        cout << "1 - Empadas de frango" << endl;
        cout << "2 - Pizza havaiana" << endl;
        cout << "3 - Pizza quatro queijos" << endl;
        cout << "4 - Pizza camponesa" << endl;
    } else if (numero_do_menu == 2) { //fast food
        cout << "5 - Caril de borrego" << endl;
        cout << "6 - Caril de frango" << endl;
        cout << "7 - Chamuças vegetarianas" << endl;
    } else if (numero_do_menu == 3) { //fast food
        cout << "8 - Bacalhau espiritual" << endl;
        cout << "9 - Lombo de porco assado com ameixas" << endl;
        cout << "10 - Polvo à lagareiro" << endl;
    }
}

void ignora_se_incompativel(){
    
        cin.clear();
        cin.ignore();
        cout << "Resposta inválida! Escolha uma das opções!" << endl; 
        
}


int escolhe_forno(){
    
    int escolha = 0;
    
    cout << "Escolha um forno" << endl;
    cout << "1 - Forno padrão" << endl;
    cout << "2 - Forno de lenha" << endl;
                
    cin >> escolha; 
    while (!cin.good() && (escolha <= 0 || escolha > 2)){
        ignora_se_incompativel();  
        cin >> escolha; 
    }
    
    return escolha;
}


void menu_dos_ingredientes(Comida * a){
    
    bool done;
    char option;
    done = false;
    
    cout << "Seleccao de ingredientes" << endl;
    
    while (!done) {
        
        cout << "O que pretende fazer?" << endl;
        cout << "A - Adicionar ingrediente" << endl;
        cout << "E - Apagar ingrediente" << endl;
        cout << "P - Imprime a lista de ingredientes" << endl;
        cout << "Q - Sair\n" << endl;
    
        cin >> option; 
        option = toupper(option); 
        
        switch (option) {
            case 'A':
            {
                cin.ignore(numeric_limits<streamsize>::max(),'\n');

		cout << "Insira o nome do ingrediente que pretende adicionar: " << endl;
                string nome_do_ingrediente;
                getline(cin, nome_do_ingrediente);
                
                cout << "Insira o tipo de ingrediente: " << endl;
                string tipo_de_ingrediente;
                getline(cin, tipo_de_ingrediente);
                
                cout << "Insira o preço em € por 100g do ingrediente: " << endl;
                float preco_do_ingrediente;
                cin >> preco_do_ingrediente;
                while (!cin.good()){
                    ignora_se_incompativel(); 
                    cin >> preco_do_ingrediente;
                }
                if(!a->procurar_um_ingrediente(nome_do_ingrediente, tipo_de_ingrediente)){
                    a->criar_ingredientes(nome_do_ingrediente, tipo_de_ingrediente, preco_do_ingrediente);
                    cout << "Ingrediente adicionado com sucesso" << endl;
                }
                else{
                   cout << "Esse ingrediente já existe" << endl;
                }
                break;
            }
            case 'E':
            {
                cin.ignore(numeric_limits<streamsize>::max(),'\n');
                
		cout << "Insira o nome do ingrediente que pretende eliminar: " << endl;
                string nome_do_ingrediente_apagar;
                getline(cin, nome_do_ingrediente_apagar);
                
                cout << "Insira o tipo de ingrediente: " << endl;
                string tipo_de_ingrediente_apagar;
                getline(cin, tipo_de_ingrediente_apagar);
                
                if(a->procurar_um_ingrediente(nome_do_ingrediente_apagar, tipo_de_ingrediente_apagar)){
                    a->apaga_um_ingrediente(nome_do_ingrediente_apagar);
                    cout << "Ingrediente eliminado com sucesso" << endl;
                }
                else{
                    cout << "Esse ingrediente não existe" << endl; 
                }
                break;
            }
            case 'P':
            {
                cout << "Lista de informações relevantes sobre os ingredientes" << endl;
		a->mostra_infos_dos_ingredientes();
                break;
            }
            case 'Q':
            {
                if(a->confirma_se_tem_pelo_menos_um_ingrediente()){
                    cout << "A gerar o preço do prato!\n"<< endl;
                    a->calcula_preco_do_prato();
                    chrono::milliseconds(2000);
                    float preco_do_prato = 0.0;
                    preco_do_prato = a->devolde_preco_prato();
                    cout << "O prato vai ficar por " << preco_do_prato << " €!" << endl;
                    done = true; //acaba ciclo
                }
                else{
                    cout << "Tem de adicionar pelo menos um ingrediente antes de poder continuar" << endl;    
                }
                break;
            }
            default:
                cout << "Resposta inválida!\n" << endl;
        } /* end switch */
    } /* end while */
}