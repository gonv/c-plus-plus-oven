/* 
 * File:   FornoLenha.h
 * Author: Marina
 *
 * Created on 19 de Junho de 2019, 15:21
 */

#ifndef FORNOLENHA_H
#define	FORNOLENHA_H

#include "forno.h"

class FornoLenha: public Forno {
      
    
    
    
public:
    FornoLenha(int capacidade_do_forno, int max_temperatura);
    
    //goal 5
    void calcula_temperatura(int diferenca, int factor) override;
    void evolucao_da_temperatura(int tempo_actual, int n_prato, int * contador_queima) override;
    
    virtual ~FornoLenha();

};

#endif	/* FORNOLENHA_H */

