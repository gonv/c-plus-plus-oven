/* 
 * File:   Ingrediente.h
 * Author: Marina
 *
 * Created on 8 de Junho de 2019, 14:54
 */

#ifndef INGREDIENTE_H
#define	INGREDIENTE_H

#include <iostream> 
#include <sstream> 
#include <string> 
#include <locale> 
#include <vector>
#include <iterator>

class Ingrediente {
    
    const std::string nome_do_ingrediente;
    const std::string tipo_de_ingrediente;
    float preco; // preço por 100g
    
public:
    Ingrediente(std::string nome, std::string tipo, float preco_venda);
    
    std::string devolve_nome_ingrediente();
    std::string devolve_tipo_ingrediente();
    float devolve_preco();
    std::string mostra_info_ingrediente();
       
    virtual ~Ingrediente();

};

#endif	/* INGREDIENTE_H */

