/* 
 * File:   FornoLenha.cpp
 * Author: Marina
 * 
 * Created on 19 de Junho de 2019, 15:21
 */

#include "fornolenha.h"

FornoLenha::FornoLenha(int capacidade_do_forno, int max_temperatura): Forno(capacidade_do_forno, max_temperatura) {
    

}


void FornoLenha::calcula_temperatura(int diferenca, int factor){
    Forno::calcula_temperatura(diferenca, ++factor);
}

void FornoLenha::evolucao_da_temperatura(int tempo_actual, int n_prato, int * contador_queima){
    Forno::evolucao_da_temperatura(tempo_actual, n_prato, contador_queima);
}

FornoLenha::~FornoLenha() {
}

