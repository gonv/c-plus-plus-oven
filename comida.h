/* 
 * File:   Comida.h
 * Author: Marina
 *
 * Created on 6 de Junho de 2019, 16:52
 */

#ifndef COMIDA_H
#define	COMIDA_H

#include "ingredientes.h"

class Comida {
    
    std::string nome_do_prato;
    std::string tipo_de_comida;
    int temperatura_confeccao;
    int temperatura_queima;
    float quantidade_de_comida;   //peso da comida em kg
    int preco_do_prato;
    std::vector<Ingrediente*> ingredientes;
    

public:
    
    Comida(std::string prato, std::string tipo, float peso, int confeccionado, int queimado);
    //Goal 4
    Comida(const Comida & a);
    
    //Goal 2
    void mostrar_prato(void);
    std::string estado_da_confeccao(int tempo, int temperatura);
    float devolve_peso_da_comida();
    
    //Goal 3
    void criar_ingredientes(std::string nome, std::string tipo, float preco_venda);
    bool procurar_um_ingrediente(std::string nome, std::string tipo);
    void apaga_um_ingrediente(std::string nome_do_ingrediente);
    void calcula_preco_do_prato();
    float devolde_preco_prato();
    void mostra_infos_dos_ingredientes();
    bool confirma_se_tem_pelo_menos_um_ingrediente();
    
    //Goal 5
    std::string devolve_nome_do_prato();
    
    ~Comida();

};
#endif	/* COMIDA_H */

